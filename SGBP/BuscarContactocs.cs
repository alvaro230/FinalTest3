﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class BuscarContactocs : Form
    {
        public BuscarContactocs()
        {
            InitializeComponent();
        }

        private void BuscarContactocs_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("Id", "Identificador");
                dataGridView1.Columns.Add("nombreC", "nombre");
                dataGridView1.Columns.Add("apellido", "apellido");
                dataGridView1.Columns.Add("codigo", "codigo");
                dataGridView1.Columns.Add("numero", "numero");

                List<Contacto> contactos = Contactos.obtenerContactosPorCodigo(BuscarViaje.id);
                foreach (Contacto contacto in contactos)
                {
                    dataGridView1.Rows.Add(contacto.id,contacto.nombreC,contacto.apellido,contacto.codigo,contacto.numero);
                }

            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }

        }
    }
}
