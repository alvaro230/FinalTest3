﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class EditarPago : FormBase
    {
        public EditarPago()
        {
            InitializeComponent();
            Iniciar_Datos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar_Datos();
                DialogResult = DialogResult.OK;
                
            }
            catch (Exception)
            {
                MessageBox.Show("No se lograron guardar los cambios.", "Aviso");
            }
        }

        private void Iniciar_Datos()
        {
           dias.Text = Properties.Settings.Default.Tolerancia.ToString();
           descuento.Text= (Properties.Settings.Default.Descuento*100).ToString();
            afp.Text = (Properties.Settings.Default.AFP * 100).ToString();
        }

        private void Guardar_Datos()
        {
            int a;
            double b,c;
            if (dias.Text != "" && descuento.Text != "" && afp.Text != "" &&
                int.TryParse(dias.Text, out a) && double.TryParse(descuento.Text, out b) && double.TryParse(afp.Text, out c))
            {
                if (Convert.ToInt32(dias.Text) >= 0 && Convert.ToDouble(descuento.Text) >= 0 && Convert.ToDouble(afp.Text) >= 0)
                {
                    Properties.Settings.Default.Tolerancia = Convert.ToInt32(dias.Text);
                    Properties.Settings.Default.Descuento = Convert.ToDouble(descuento.Text) / 100;
                    Properties.Settings.Default.AFP = Convert.ToDouble(afp.Text) / 100;
                    Properties.Settings.Default.Save();
                    MessageBox.Show("Se guardaron los cambios", "Aviso");
                }else
                {
                    MessageBox.Show("No puede ingresar valores menores a cero", "Aviso");
                }
            }
            else
            {
                MessageBox.Show("Uno o varios datos ingresados son incorrectos.", "Aviso");
            }
        }
    }
}
