﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;


namespace SGBP
{
    public partial class MostrarPapeletaFijo : FormBase
    {
        public string Ci="";
        public string dia1 = "";
        public string mes1 = "";
        public string año1 = "";
        public string dia2 = "";
        public string mes2 = "";
        public string año2 = "";
        public string id = "";
        public MostrarPapeletaFijo(string Ci,string dia1,string mes1,string año1,string dia2,string mes2,string año2)
        {
            InitializeComponent();
            this.Ci = Ci;
            this.dia1 = dia1;
            this.mes1 = mes1;
            this.año1 = año1;
            this.dia2 = dia2;
            this.mes2 = mes2;
            this.año2 = año2;
        }


      
        private void MostrarPapeleta_Load(object sender, EventArgs e)
        {

            try
            {
                string fec = string.Format(dia1 + signoFecha + mes1 + signoFecha + año1);
                string fec2 = string.Format(dia2 + signoFecha + mes2 + signoFecha + año2);
                Empleado empleado = Empleados.obtenerEmpleadoPorCi(Ci);
       
                List<Papeleta> papeletas = Papeletas.ObtenerPapeletasDeEmpleado(Ci);
                foreach(Papeleta p in papeletas)
                {
                    if(p.FechaInicio==fec && p.FechaFin == fec2)
                    {
                        id = p.Ci;
                        fecha1.Text = p.FechaInicio;
                        fecha2.Text = p.FechaFin;
                        cargo.Text = p.Cargo;
                        salario.Text = p.Salario;
                        des1.Text = p.Descuentos;
                        des2.Text = p.AFP;
                        total.Text = p.Pago;
                    }
                }
                
                nom.Text = empleado.Nombre;
                ape1.Text = empleado.Apellido1;
                ape2.Text = empleado.Apellido2;
                
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message,"Avisoo");
            }
            
        }

        private void ape1_Click(object sender, EventArgs e)
        {

        }

        private void ape2_Click(object sender, EventArgs e)
        {

        }

        private void nom_Click(object sender, EventArgs e)
        {

        }

        private void cargo_Click(object sender, EventArgs e)
        {

        }

        private void fecha_Click(object sender, EventArgs e)
        {

        }

        private void btnimprimir_Click(object sender, EventArgs e)
        {
            
            GestorPapeletas.PdfFijo(Ci, nom.Text, ape1.Text, ape2.Text, cargo.Text, salario.Text, des1.Text, des2.Text,
                total.Text, fecha1.Text, fecha2.Text);
             
        }
    }
}
