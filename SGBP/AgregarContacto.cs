﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class AgregarContacto : Form
    {
        public AgregarContacto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                Contacto contacto = new Contacto(nom.Text.Trim(), apell.Text.Trim(), BuscarViaje.id, num.Text.Trim());
                Contactos.crearContacto(contacto);
                MessageBox.Show("se registro exitosamente el contacto");
                this.Close();
             }
            catch(Exception er)
            {
                MessageBox.Show(er.Message);
            }

       }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
