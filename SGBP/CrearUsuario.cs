﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class CrearUsuario : FormBase
    {
        public CrearUsuario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cuentatxt.Text.Length > 0 && passtxt.Text.Length > 0)
                {
                    Empleado emp = new Empleado("xyz123", "x", "xx", "xxx", "xxxx", "05/05/1999", "0", "1", "0", "0", "0", "0");
                    Empleados.crearEmpleado(emp);
                    Usuario us = new Usuario(cuentatxt.Text.Trim(), passtxt.Text.Trim(), "1", "xyz123");
                    Usuarios.crearUsuario(us);
                    MessageBox.Show("Gracias ahora puede entrar al sistema con el usuario que acaba de crear", "Aviso");
                    DialogResult = DialogResult.OK;
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Nombre de usuario y contraseña no pueden estar vacios.", "Aviso");
                }
            }
            catch (Exception )
            {
                
            }
            
        }

        private void CrearUsuario_Load(object sender, EventArgs e)
        {

        }
    }
}
