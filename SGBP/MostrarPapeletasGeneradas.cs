﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class MostrarPapeletasGeneradas : FormBase
    {
       
        List<Papeleta> papeletasGeneradas=null;
        public MostrarPapeletasGeneradas(List<Papeleta> papeletas)
        {
            InitializeComponent();
            this.papeletasGeneradas = papeletas;
            
        }
       

        private void MostrarPapeletasGeneradas_Load(object sender, EventArgs e)
        {
            try
            {
               
                dataGridView1.Columns.Add("Empleado_ci", "Ci");
                dataGridView1.Columns.Add("Nom", "Nombre");
                dataGridView1.Columns.Add("Ape1", "Apellido Paterno");
                dataGridView1.Columns.Add("Ape2", "Apellido Materno");
                dataGridView1.Columns.Add("Fec", "Fecha inicial");
                dataGridView1.Columns.Add("Fec2", "Fecha final");
                if (papeletasGeneradas != null)
                {
                    foreach (Papeleta p in papeletasGeneradas)
                    {
                        Empleado empleado = Empleados.obtenerEmpleadoPorCi(p.Ci);
                       
                            dataGridView1.Rows.Add(empleado.Ci,empleado.Nombre,empleado.Apellido1,empleado.Apellido2,
                                p.FechaInicio, p.FechaFin);
                    }
                }
               
               
            }
            catch (Exception)
            {
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
             
            if (MessageBox.Show("Confirma la impresion de " +
               "\ntodas las papeletas generadas? ", "Aviso", MessageBoxButtons.YesNo
               , MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (papeletasGeneradas != null)
                {
                    
                    string fecini = papeletasGeneradas.First().FechaInicio;
                    string fecfin = papeletasGeneradas.First().FechaFin;
                    
                    FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                    // Show the FolderBrowserDialog.
                    DialogResult result = folderBrowserDialog1.ShowDialog();
                    string folderName = "";
                    if (result == DialogResult.OK)
                    {
                        folderName = folderBrowserDialog1.SelectedPath + "/";
                        //dando formato al pdf
                        //instanciando un documento
                        Document doc = new Document(iTextSharp.text.PageSize.LETTER, 30, 30, 10, 10);
                        //dando tamaño
                        iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(450, 300);
                        doc.SetPageSize(rec);
                        //definiendo tipo
                        PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(@folderName + "Papeletas_Empleados_" + fecfin.Substring(3,2) + fecfin.Substring(6,4) + ".pdf", FileMode.Create));
                        //definiendo fuentes, estilos
                        iTextSharp.text.Font titulofuente =
                            new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15, 1);
                        iTextSharp.text.Font textofuente =
                            new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10);
                        //abriendo doc
                        doc.Open();
                        foreach (Papeleta p in papeletasGeneradas)
                        {
                            
                                Empleado emp = Empleados.obtenerEmpleadoPorCi(p.Ci);
                                Paragraph titulo = new Paragraph("ASOCIADOS INFORMATICOS\nPapeleta de pago\n", titulofuente);
                            //alineacion centro
                            titulo.Alignment = Element.ALIGN_CENTER;
                                
                                Paragraph parrafo1 = new Paragraph
                                    ("Empleado                                             "
                                            + "Periodo: Del " + fecini + " Al " + fecfin, textofuente);
                                Paragraph parrafo2 = new Paragraph
                                    (emp.Nombre + "     " +
                                    emp.Apellido1 + "     " +
                                    emp.Apellido2 + "       " +
                                    "Cargo: " + p.Cargo + "\n\n", textofuente);
                                Paragraph parrafo3 = new Paragraph
                                    ("Salario basico:               " + p.Salario + "\n\n", textofuente);
                                Paragraph parrafo4 = new Paragraph
                                    ("", textofuente);
                            //si empleado es por hora 0 imprimir horas tambien
                                if (!Convert.ToBoolean(emp.Tipo))
                                {

                                    int horas = Papeletas.calcularHoras
                                        (Asistencias.obtenerAsistenciasDeEmpleado(emp.id),
                                        Papeletas.stringAfecha(p.FechaInicio),
                                        Papeletas.stringAfecha(p.FechaFin));
                                    parrafo4 = new Paragraph
                                    ("Horas Trabajadas:           " + horas + "\n\n", textofuente);
                                }
                                Paragraph parrafo5 = new Paragraph
                                    ("Descuentos:                   " + p.Descuentos + "\n\n", textofuente);
                                Paragraph parrafo6 = new Paragraph
                                    ("AFP:                           " + p.AFP + "\n\n", textofuente);
                                Paragraph parrafo7 = new Paragraph
                                    ("Pago total:                   " + p.Pago + "\n", textofuente);

                                doc.Add(titulo);
                                doc.Add(parrafo1);
                                doc.Add(parrafo2);
                                doc.Add(parrafo3);
                                doc.Add(parrafo4);
                                doc.Add(parrafo5);
                                doc.Add(parrafo6);
                                doc.Add(parrafo7);
                                doc.NewPage();
                            
                        }
                        doc.Close();
                    }else
                    {
                        //no se selecciono directorio
                    }
                }
                else
                {
                    MessageBox.Show("No se genero ninguna papeleta.", "Aviso");
                }
                   
            }
            else
            {
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("Empleado_ci", "Ci");
                dataGridView1.Columns.Add("Nom", "Nombre");
                dataGridView1.Columns.Add("Ape1", "Apellido Paterno");
                dataGridView1.Columns.Add("Ape2", "Apellido Materno");
                dataGridView1.Columns.Add("Fec", "Fecha inicial");
                dataGridView1.Columns.Add("Fec2", "Fecha final");
                if (citxt.Text != "")
                {
                    
                   
                    List<Papeleta> papeletas = Papeletas.obtenerPapeletasPorEmpleadoParecido(citxt.Text);
                    foreach (Papeleta p in papeletas)
                    {
                        
                        foreach(Papeleta papeleta in papeletasGeneradas)
                        {
                            if(papeleta.Ci==p.Ci && papeleta.FechaInicio==p.FechaInicio && papeleta.FechaFin==p.FechaFin)
                            {
                                Empleado empleado = Empleados.obtenerEmpleadoPorCi(p.Ci);
                                dataGridView1.Rows.Add(papeleta.Ci, empleado.Nombre, empleado.Apellido1, empleado.Apellido2,
                                        papeleta.FechaInicio, papeleta.FechaFin);
                            }
                        }

                        
                    }

                   
                }
                else
                {
                    foreach(Papeleta p in papeletasGeneradas)
                    {
                        Empleado empleado = Empleados.obtenerEmpleadoPorCi(p.Ci);
                        dataGridView1.Rows.Add(p.Ci, empleado.Nombre, empleado.Apellido1, empleado.Apellido2,
                                p.FechaInicio, p.FechaFin);
                    }
                }
        
            }
            catch (Exception)
            {
             
            }

        }
            
   
        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Selected)
                {
                    string ciemp=dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                    
                    Empleado empleado = Empleados.obtenerEmpleadoPorCi(ciemp);
                    string fechaini = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].Value.ToString();
                    string fechafin = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].Value.ToString();
                    string dia1, mes1, año1,dia2,mes2,año2;
                    dia1 = "" + fechaini[0] + fechaini[1];
                    mes1 = "" + fechaini[3] + fechaini[4];
                    año1 = "" + fechaini[6] + fechaini[7] + fechaini[8] + fechaini[9];
                    dia2 = "" + fechafin[0] + fechafin[1];
                    mes2 = "" + fechafin[3] + fechafin[4];
                    año2 = "" + fechafin[6] + fechafin[7] + fechafin[8] + fechafin[9];
                    if (Convert.ToBoolean(empleado.Tipo))
                    {

                        MostrarPapeletaFijo memp = new MostrarPapeletaFijo(ciemp, dia1, mes1, año1, dia2, mes2, año2);
                        this.Hide();
                        memp.ShowDialog();
                        this.Show();
                    }
                    else
                    {
                        MostrarPapeletaHora memp = new MostrarPapeletaHora(ciemp, dia1, mes1, año1, dia2, mes2, año2);
                        this.Hide();
                        memp.ShowDialog();
                        this.Show();
                    }
                }
                else
                {
                    MessageBox.Show("No seleccion una papeleta.", "Aviso");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No seleccion una papeleta.","Aviso");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
