﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class CrearPapeletas : FormBase
    {
        public CrearPapeletas()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Confirma la creacion de papeletas " +
               "\npara todos los empleados? ", "Aviso", MessageBoxButtons.YesNo
               , MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    string fecha1 = datepick.Value.Date.ToString("dd/MM/yyyy");
                    string fecha2 = datepick2.Value.Date.ToString("dd/MM/yyyy");
                    string dia1 = "" + fecha1[0] + fecha1[1];
                    string mes1 = "" + fecha1[3] + fecha1[4];
                    string año1 = "" + fecha1[6] + fecha1[7] + fecha1[8] + fecha1[9];
                    string dia2 = "" + fecha2[0] + fecha2[1];
                    string mes2 = "" + fecha2[3] + fecha2[4];
                    string año2 = "" + fecha2[6] + fecha2[7] + fecha2[8] + fecha2[9];
                    if (dia1!= "" && mes1!="" && año1!="" && dia2!= "" && mes2 != "" && año2 != "")
                    {

                        List<Papeleta> papeletas=Papeletas.crearPapeletasParaTodos(dia1,mes1,año1,dia2,mes2, año2);

                        
                        MostrarPapeletasGeneradas pge = new MostrarPapeletasGeneradas(papeletas);
                           
                        pge.ShowDialog();
                        
                        CrearPapeletas_Load(sender, e);
                    }else
                    {
                        MessageBox.Show("Debe ingresar los datos correspondientes\npara generar las papeletas");
                    }
                }
               
            }catch(Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {

        }

        

        private void CrearPapeletas_Load(object sender, EventArgs e)
        {
            List<Papeleta> papeletas = Papeletas.ObtenerTodasLasPapeletas();
            if (papeletas.Count > 0)
            {
                datepick.Text = papeletas.First().FechaFin;
            }
        }
    }
}
