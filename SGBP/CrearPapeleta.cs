﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class CrearPapeleta : FormBase
    {
        public CrearPapeleta()
        {
            InitializeComponent();
        }

        public static string Ci = "";
       
        
        private void CrearPapeleta_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Columns.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Add("Ci", "Ci");
                dataGridView1.Columns.Add("Nombre", "Nombre");
                dataGridView1.Columns.Add("Apellido1", "Apellido Paterno");
                dataGridView1.Columns.Add("Apellido2", "Apellido Materno");
                
                List<Empleado> empleados = Empleados.obtenerEmpleados();
                List<Papeleta> papeletas = Papeletas.ObtenerTodasLasPapeletas();
                if (papeletas.Count > 0)
                {
                    datepick.Text = papeletas.First().FechaFin;
                }
                foreach (Empleado emp in empleados)
                {
                    dataGridView1.Rows.Add(emp.Ci, emp.Nombre, emp.Apellido1, emp.Apellido2);
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma la creacion de papeleta "+
                "\npara el empleado", "Aviso", MessageBoxButtons.YesNo
                , MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
               
                try
                {
                   
                    if (dataGridView1.CurrentRow.Selected)
                    {
                        Ci = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                        string fecha1 = datepick.Value.Date.ToString("dd/MM/yyyy");
                        string fecha2 = datepick2.Value.Date.ToString("dd/MM/yyyy");
                        string dia1 = "" + fecha1[0] + fecha1[1];
                        string mes1 = "" + fecha1[3] + fecha1[4];
                        string año1 = "" + fecha1[6] + fecha1[7] + fecha1[8] + fecha1[9];
                        string dia2 = "" + fecha2[0] + fecha2[1];
                        string mes2 = "" + fecha2[3] + fecha2[4];
                        string año2 = "" + fecha2[6] + fecha2[7] + fecha2[8] + fecha2[9];
                        if (dia1 != "" && mes1 != "" && año1 != "" &&
                            dia2 != "" && mes2 != "" && año2 != "")
                        {
                            Empleado empleado = Empleados.obtenerEmpleadoPorCi(Ci);

                            if (GestorPapeletas.validarCreacionPapeleta(Ci, dia1, mes1, año1,
                                dia2, mes2, año2))
                            {
                                //Ci = empleado.Ci;
                                if (Convert.ToBoolean(empleado.Tipo))
                                {
                                    Papeletas.crearPapeletaFijo
                                        (Ci, dia1, mes1, año1,
                                        dia2, mes2, año2);

                                    this.Hide();

                                    MostrarPapeletaFijo mpa = new MostrarPapeletaFijo
                                        (Ci, dia1, mes1, año1,
                                        dia2, mes2, año2);

                                    mpa.ShowDialog();
                                    //this.Close();
                                     CrearPapeleta_Load(sender,new EventArgs());
                                   
                                }
                                else
                                {
                                    Papeletas.crearPapeletaHora
                                       (Ci, dia1, mes1, año1,
                                        dia2, mes2, año2);

                                    this.Hide();

                                    MostrarPapeletaHora mpa = new MostrarPapeletaHora
                                       (Ci, dia1, mes1, año1,
                                        dia2, mes2, año2);

                                    mpa.ShowDialog();
                                    //this.Close();
                                    CrearPapeleta_Load(sender, new EventArgs());
                                    
                                }
                            }
                            else
                            {
                                MessageBox.Show("No se puede generar la papeleta de pago para este empleado\n"+
                                    "las posibles causas son:\n"+
                                    "1. Ya se genero la papeleta de pago dentro del periodo para el empleado\n"+
                                    "2. El empleado no tiene un registro de asistencias dentro del periodo","Aviso");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Uno o varios campos estan vacios o son incorrectos", "Aviso");
                        }
                    }
                    else
                    {
                        MessageBox.Show("No hay empleado seleccionado para generar la papeleta", "Aviso");
                    }
                }
                catch (Exception)
                {
                   
                }
                
            }
            else
            {
                return;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("Ci", "Ci");
                dataGridView1.Columns.Add("Nombre", "Nombre");
                dataGridView1.Columns.Add("Apellido1", "Apellido Paterno");
                dataGridView1.Columns.Add("Apellido2", "Apellido Materno");
                List<Empleado> empleados = Empleados.obtenerEmpleadosPorCiParecido(citxt.Text.Trim());
                foreach (Empleado emp in empleados)
                {
                    dataGridView1.Rows.Add(emp.Ci, emp.Nombre, emp.Apellido1, emp.Apellido2);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("No se encontro al empleado con ese Ci");
            }
        }
    }
}
