﻿namespace SGBP
{
    partial class AgregarContacto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nombre = new System.Windows.Forms.Label();
            this.apellido = new System.Windows.Forms.Label();
            this.codigo = new System.Windows.Forms.Label();
            this.numero = new System.Windows.Forms.Label();
            this.apell = new System.Windows.Forms.TextBox();
            this.num = new System.Windows.Forms.TextBox();
            this.nom = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nombre
            // 
            this.nombre.AutoSize = true;
            this.nombre.Location = new System.Drawing.Point(65, 43);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(42, 13);
            this.nombre.TabIndex = 0;
            this.nombre.Text = "nombre";
            // 
            // apellido
            // 
            this.apellido.AutoSize = true;
            this.apellido.Location = new System.Drawing.Point(66, 83);
            this.apellido.Name = "apellido";
            this.apellido.Size = new System.Drawing.Size(43, 13);
            this.apellido.TabIndex = 1;
            this.apellido.Text = "apellido";
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(66, 120);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(0, 13);
            this.codigo.TabIndex = 2;
            // 
            // numero
            // 
            this.numero.AutoSize = true;
            this.numero.Location = new System.Drawing.Point(65, 120);
            this.numero.Name = "numero";
            this.numero.Size = new System.Drawing.Size(42, 13);
            this.numero.TabIndex = 3;
            this.numero.Text = "numero";
            // 
            // apell
            // 
            this.apell.Location = new System.Drawing.Point(130, 83);
            this.apell.Name = "apell";
            this.apell.Size = new System.Drawing.Size(139, 20);
            this.apell.TabIndex = 4;
            // 
            // num
            // 
            this.num.Location = new System.Drawing.Point(130, 120);
            this.num.Name = "num";
            this.num.Size = new System.Drawing.Size(139, 20);
            this.num.TabIndex = 6;
            // 
            // nom
            // 
            this.nom.Location = new System.Drawing.Point(130, 43);
            this.nom.Name = "nom";
            this.nom.Size = new System.Drawing.Size(139, 20);
            this.nom.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(68, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "registrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(207, 240);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "cancelar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AgregarContacto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(468, 318);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nom);
            this.Controls.Add(this.num);
            this.Controls.Add(this.apell);
            this.Controls.Add(this.numero);
            this.Controls.Add(this.codigo);
            this.Controls.Add(this.apellido);
            this.Controls.Add(this.nombre);
            this.Name = "AgregarContacto";
            this.Text = "AgregarContacto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nombre;
        private System.Windows.Forms.Label apellido;
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.Label numero;
        private System.Windows.Forms.TextBox apell;
        private System.Windows.Forms.TextBox num;
        private System.Windows.Forms.TextBox nom;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}