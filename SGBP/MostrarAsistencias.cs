﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class MostrarAsistencias : FormBase
    {
        public MostrarAsistencias()
        {
            InitializeComponent();
           
        }

        private void MostrarAsistencias_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("ID", "Identificador");
                dataGridView1.Columns.Add("Nombre", "Nombre");
                dataGridView1.Columns.Add("Apellido1", "Apellido Paterno");
                dataGridView1.Columns.Add("Apellido2", "Apellido Materno");
                dataGridView1.Columns.Add("Fecha", "Fecha");
                dataGridView1.Columns.Add("Hora", "Hora");
                dataGridView1.Columns.Add("Tipo", "ENT/SAL");
                List<Asistencia> ls = new List<Asistencia>();
                List<Asistencia> ls2 = new List<Asistencia>();
                string fec;
               
                    ls = Asistencias.obtenerAsistencias();
                    if (ls.Count > 0)
                    {
                        fec = ls.First().Fecha;
                        ls2 = Asistencias.obtenerAsistenciasPorMes(fec.Substring(3, 2), fec.Substring(6, 4));
                        mes1.Text = fec.Substring(3, 2);
                        año1.Text = fec.Substring(6, 4);
                    }
                
              
                    foreach (Asistencia a in ls2)
                    {
                        Empleado emp = Empleados.obtenerEmpleadoPorIdAsistencia(a.Id);
                        dataGridView1.Rows.Add(a.Id, emp.Nombre, emp.Apellido1, emp.Apellido2, a.Fecha, a.Hora, a.Tipo);
                    }
                
            }
            catch(Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("ID", "Identificador");
                dataGridView1.Columns.Add("Nombre", "Nombre");
                dataGridView1.Columns.Add("Apellido1", "Apellido Paterno");
                dataGridView1.Columns.Add("Apellido2", "Apellido Materno");
                dataGridView1.Columns.Add("Fecha", "Fecha");
                dataGridView1.Columns.Add("Hora", "Hora");
                dataGridView1.Columns.Add("Tipo", "ENT/SAL");
                
                List<Asistencia> ls2 = new List<Asistencia>();
                int n;
                
                    if (mes1.Text == "" || año1.Text == "")
                    {
                        if (Asistencias.id == "")
                        {
                            ls2 = Asistencias.obtenerAsistencias();
                        }
                        else
                        {
                            ls2 = Asistencias.obtenerAsistenciasDeEmpleado(Asistencias.id);
                        }
                    }
                    else
                    {
                        if (int.TryParse(mes1.Text.Trim(), out n) && int.TryParse(año1.Text.Trim(), out n))
                        {
                            ls2 = Asistencias.obtenerAsistenciasPorMes(mes1.Text, año1.Text);
                        }
                        else
                        {
                            MessageBox.Show("Datos de mes o año incorrectos", "Aviso"); 
                        }
                    }


                    foreach (Asistencia a in ls2)
                    {
                        Empleado emp = Empleados.obtenerEmpleadoPorIdAsistencia(a.Id);
                        dataGridView1.Rows.Add(a.Id, emp.Nombre, emp.Apellido1, emp.Apellido2, a.Fecha, a.Hora, a.Tipo);
                    }
                    ls2.Clear();
                
           
                
            }
            catch(Exception)
            {
               
            }
        }
    }
}
