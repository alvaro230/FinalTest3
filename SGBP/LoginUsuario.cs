﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class LoginUsuario : Form
    {
        public LoginUsuario()
        {
            InitializeComponent();
        }

        private void agregarViajeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearViaje a = new CrearViaje();
            a.Show();
        }

        private void LoginUsuario_Load(object sender, EventArgs e)
        {

        }

        private void mostrarViajeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarViaje viaje = new BuscarViaje();
            viaje.Show();
        }
    }
}
