﻿namespace SGBP
{
    partial class MostrarPapeletaHora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cargo = new System.Windows.Forms.Label();
            this.total23 = new System.Windows.Forms.Label();
            this.des242 = new System.Windows.Forms.Label();
            this.des12 = new System.Windows.Forms.Label();
            this.salario42 = new System.Windows.Forms.Label();
            this.labell = new System.Windows.Forms.Label();
            this.ape2 = new System.Windows.Forms.Label();
            this.ape1 = new System.Windows.Forms.Label();
            this.nom = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label332 = new System.Windows.Forms.Label();
            this.fecha2 = new System.Windows.Forms.Label();
            this.des2 = new System.Windows.Forms.Label();
            this.des1 = new System.Windows.Forms.Label();
            this.horas = new System.Windows.Forms.Label();
            this.salario = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.btnimprimir = new System.Windows.Forms.Button();
            this.fecha1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btnsalir
            // 
            this.btnsalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Location = new System.Drawing.Point(580, 522);
            this.btnsalir.Size = new System.Drawing.Size(112, 32);
            this.btnsalir.Text = "Volver";
            // 
            // cargo
            // 
            this.cargo.AutoSize = true;
            this.cargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cargo.Location = new System.Drawing.Point(748, 135);
            this.cargo.Name = "cargo";
            this.cargo.Size = new System.Drawing.Size(66, 24);
            this.cargo.TabIndex = 25;
            this.cargo.Text = "Cargo:";
            // 
            // total23
            // 
            this.total23.AutoSize = true;
            this.total23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total23.Location = new System.Drawing.Point(42, 454);
            this.total23.Name = "total23";
            this.total23.Size = new System.Drawing.Size(97, 24);
            this.total23.TabIndex = 24;
            this.total23.Text = "Pago total:";
            // 
            // des242
            // 
            this.des242.AutoSize = true;
            this.des242.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.des242.Location = new System.Drawing.Point(42, 392);
            this.des242.Name = "des242";
            this.des242.Size = new System.Drawing.Size(52, 24);
            this.des242.TabIndex = 23;
            this.des242.Text = "AFP:";
            // 
            // des12
            // 
            this.des12.AutoSize = true;
            this.des12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.des12.Location = new System.Drawing.Point(42, 334);
            this.des12.Name = "des12";
            this.des12.Size = new System.Drawing.Size(115, 24);
            this.des12.TabIndex = 22;
            this.des12.Text = "Descuentos:";
            // 
            // salario42
            // 
            this.salario42.AutoSize = true;
            this.salario42.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salario42.Location = new System.Drawing.Point(42, 217);
            this.salario42.Name = "salario42";
            this.salario42.Size = new System.Drawing.Size(132, 24);
            this.salario42.TabIndex = 21;
            this.salario42.Text = "Salario basico:";
            // 
            // labell
            // 
            this.labell.AutoSize = true;
            this.labell.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labell.Location = new System.Drawing.Point(658, 135);
            this.labell.Name = "labell";
            this.labell.Size = new System.Drawing.Size(66, 24);
            this.labell.TabIndex = 20;
            this.labell.Text = "Cargo:";
            // 
            // ape2
            // 
            this.ape2.AutoSize = true;
            this.ape2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ape2.Location = new System.Drawing.Point(435, 135);
            this.ape2.Name = "ape2";
            this.ape2.Size = new System.Drawing.Size(97, 24);
            this.ape2.TabIndex = 17;
            this.ape2.Text = "Empleado";
            // 
            // ape1
            // 
            this.ape1.AutoSize = true;
            this.ape1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ape1.Location = new System.Drawing.Point(227, 135);
            this.ape1.Name = "ape1";
            this.ape1.Size = new System.Drawing.Size(97, 24);
            this.ape1.TabIndex = 16;
            this.ape1.Text = "Empleado";
            // 
            // nom
            // 
            this.nom.AutoSize = true;
            this.nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nom.Location = new System.Drawing.Point(42, 135);
            this.nom.Name = "nom";
            this.nom.Size = new System.Drawing.Size(97, 24);
            this.nom.TabIndex = 15;
            this.nom.Text = "Empleado";
            this.nom.Click += new System.EventHandler(this.nom_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 24);
            this.label2.TabIndex = 14;
            this.label2.Text = "Empleado:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(281, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(419, 32);
            this.label1.TabIndex = 13;
            this.label1.Text = "ASOCIADOS INFORMATICOS";
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label332.Location = new System.Drawing.Point(42, 276);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(155, 24);
            this.label332.TabIndex = 26;
            this.label332.Text = "Horas trabajadas:";
            // 
            // fecha2
            // 
            this.fecha2.AutoSize = true;
            this.fecha2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha2.Location = new System.Drawing.Point(748, 93);
            this.fecha2.Name = "fecha2";
            this.fecha2.Size = new System.Drawing.Size(81, 24);
            this.fecha2.TabIndex = 31;
            this.fecha2.Text = "Periodo:";
            // 
            // des2
            // 
            this.des2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.des2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.des2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.des2.Location = new System.Drawing.Point(210, 390);
            this.des2.Name = "des2";
            this.des2.Size = new System.Drawing.Size(581, 28);
            this.des2.TabIndex = 30;
            this.des2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // des1
            // 
            this.des1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.des1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.des1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.des1.Location = new System.Drawing.Point(210, 332);
            this.des1.Name = "des1";
            this.des1.Size = new System.Drawing.Size(581, 28);
            this.des1.TabIndex = 29;
            this.des1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // horas
            // 
            this.horas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.horas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.horas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.horas.Location = new System.Drawing.Point(210, 274);
            this.horas.Name = "horas";
            this.horas.Size = new System.Drawing.Size(581, 28);
            this.horas.TabIndex = 28;
            this.horas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // salario
            // 
            this.salario.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.salario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.salario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salario.Location = new System.Drawing.Point(210, 215);
            this.salario.Name = "salario";
            this.salario.Size = new System.Drawing.Size(581, 28);
            this.salario.TabIndex = 27;
            this.salario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // total
            // 
            this.total.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.Location = new System.Drawing.Point(210, 452);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(581, 28);
            this.total.TabIndex = 32;
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnimprimir
            // 
            this.btnimprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnimprimir.Location = new System.Drawing.Point(308, 522);
            this.btnimprimir.Name = "btnimprimir";
            this.btnimprimir.Size = new System.Drawing.Size(112, 32);
            this.btnimprimir.TabIndex = 33;
            this.btnimprimir.Text = "Imprimir";
            this.toolTip1.SetToolTip(this.btnimprimir, "Crear archivo pdf\r\nde la papeleta de pago.");
            this.btnimprimir.UseVisualStyleBackColor = true;
            this.btnimprimir.Click += new System.EventHandler(this.btnimprimir_Click);
            // 
            // fecha1
            // 
            this.fecha1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha1.Location = new System.Drawing.Point(538, 93);
            this.fecha1.Name = "fecha1";
            this.fecha1.Size = new System.Drawing.Size(101, 24);
            this.fecha1.TabIndex = 37;
            this.fecha1.Text = "Fecha";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(715, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 24);
            this.label5.TabIndex = 36;
            this.label5.Text = "Al";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(494, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 24);
            this.label4.TabIndex = 35;
            this.label4.Text = "Del";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(393, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 24);
            this.label3.TabIndex = 34;
            this.label3.Text = "Periodo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(380, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 32);
            this.label6.TabIndex = 38;
            this.label6.Text = "Papeleta de pago";
            // 
            // MostrarPapeletaHora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(960, 593);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.fecha1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnimprimir);
            this.Controls.Add(this.total);
            this.Controls.Add(this.fecha2);
            this.Controls.Add(this.des2);
            this.Controls.Add(this.des1);
            this.Controls.Add(this.horas);
            this.Controls.Add(this.salario);
            this.Controls.Add(this.label332);
            this.Controls.Add(this.cargo);
            this.Controls.Add(this.total23);
            this.Controls.Add(this.des242);
            this.Controls.Add(this.des12);
            this.Controls.Add(this.salario42);
            this.Controls.Add(this.labell);
            this.Controls.Add(this.ape2);
            this.Controls.Add(this.ape1);
            this.Controls.Add(this.nom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MostrarPapeletaHora";
            this.Text = "Papeleta de empleado por hora";
            this.Load += new System.EventHandler(this.MostrarPapeletaHora_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.nom, 0);
            this.Controls.SetChildIndex(this.ape1, 0);
            this.Controls.SetChildIndex(this.ape2, 0);
            this.Controls.SetChildIndex(this.labell, 0);
            this.Controls.SetChildIndex(this.salario42, 0);
            this.Controls.SetChildIndex(this.des12, 0);
            this.Controls.SetChildIndex(this.des242, 0);
            this.Controls.SetChildIndex(this.total23, 0);
            this.Controls.SetChildIndex(this.cargo, 0);
            this.Controls.SetChildIndex(this.label332, 0);
            this.Controls.SetChildIndex(this.salario, 0);
            this.Controls.SetChildIndex(this.horas, 0);
            this.Controls.SetChildIndex(this.des1, 0);
            this.Controls.SetChildIndex(this.des2, 0);
            this.Controls.SetChildIndex(this.fecha2, 0);
            this.Controls.SetChildIndex(this.total, 0);
            this.Controls.SetChildIndex(this.btnimprimir, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.fecha1, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.btnsalir, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cargo;
        private System.Windows.Forms.Label total23;
        private System.Windows.Forms.Label des242;
        private System.Windows.Forms.Label des12;
        private System.Windows.Forms.Label salario42;
        private System.Windows.Forms.Label labell;
        private System.Windows.Forms.Label ape2;
        private System.Windows.Forms.Label ape1;
        private System.Windows.Forms.Label nom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.Label fecha2;
        private System.Windows.Forms.Label des2;
        private System.Windows.Forms.Label des1;
        private System.Windows.Forms.Label horas;
        private System.Windows.Forms.Label salario;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label fecha1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Button btnimprimir;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}