﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class BuscarViaje : Form
    {

        public static string id = " ";

        public BuscarViaje()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Selected)
                {
                    id = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                    //this.Hide();

                    MostrarViaje memp = new MostrarViaje();
                    memp.ShowDialog();
                    BuscarViaje_Load(sender, new EventArgs());
                    this.Show();
                    // memp.ShowDialog();

                }
                else
                {
                    MessageBox.Show("No selecciono un viaje.", "Aviso");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No selecciono un empleado.", "Aviso");
            }
        }

        private void BuscarViaje_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("Id", "Identificador");
                dataGridView1.Columns.Add("nombre", "nombre");
                dataGridView1.Columns.Add("Empleado_ci", "Empleado_ci");
                
                List<Viaje> viajes = Viajes.obtenerViajes();
                foreach (Viaje viaje in viajes)
                {
                    dataGridView1.Rows.Add(viaje.Id,viaje.nombre, viaje.Empleado_ci);
                }

            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void agregarcon_Click(object sender, EventArgs e)
        {

            try
            {
                if (dataGridView1.CurrentRow.Selected)
                {
                    id = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                    //this.Hide();

                    AgregarContacto memp = new AgregarContacto();
                    memp.ShowDialog();
                    BuscarViaje_Load(sender, new EventArgs());
                    this.Show();
                    // memp.ShowDialog();

                }
                else
                {
                    MessageBox.Show("No selecciono un viaje.", "Aviso");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No selecciono un empleado.", "Aviso");
            }
        }


    }

}
