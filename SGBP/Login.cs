﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

 
namespace SGBP
{
    public partial class Login : FormBase
    {
        public Login()
        {
            InitializeComponent();
            Iniciar_Datos();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                List<Usuario> usuarios = Usuarios.obtenerUsuarios();
                if (usuarios.Count<1 || !GestorUsuarios.existeAdministradores())
                {
                    CrearUsuario cu = new CrearUsuario();
                    this.Hide();
                    cu.ShowDialog();
                    this.Show();
                }
            }
            catch(Exception)
            {
                this.Close();
            }
            
        }

        public static string Codigo = ""; 

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                Usuario usuario = Usuarios.obtenerUsuario(cuenta.Text.Trim(),contraseña.Text.Trim());
                GestorUsuarios.usuarioEmpleadoCi= usuario.Empleado_ci;
                if (GestorUsuarios.validarIngresoUsuario(usuario, cuenta.Text.Trim(), contraseña.Text.Trim()))
                {

                    Guardar_Datos();
                    if (!checkBox1.Checked)
                    {
                       contraseña.Text = "";
                    }
                    
                    if (Convert.ToBoolean(usuario.Rol))
                    {
                        LoginUsuario ladmi = new LoginUsuario();
                        this.Hide();

                        ladmi.ShowDialog();
                        this.Show();
                    }
                  
                }
               
            }
            catch(Exception)
            {
                MessageBox.Show("nombre de usuario y/o contraseña incorrectos","Aviso");
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

      

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            Application.Exit();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

       
        private void contraseña_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(this, new EventArgs());
                
            }
        }

        private void Iniciar_Datos()
        {
            if (Properties.Settings.Default.Usuario != string.Empty)
            {
                if (Properties.Settings.Default.Recordar == "yes")
                {
                    cuenta.Text = Properties.Settings.Default.Usuario;
                    contraseña.Text = Properties.Settings.Default.Contraseña;
                    checkBox1.Checked = true;
                }/*else
                {
                    cuenta.Text = Properties.Settings.Default.Usuario;
                }*/
            }
        }

        private void Guardar_Datos()
        {
            if (checkBox1.Checked)
            {
                Properties.Settings.Default.Usuario = cuenta.Text;
                Properties.Settings.Default.Contraseña = contraseña.Text;
                Properties.Settings.Default.Recordar = "yes";
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.Usuario = cuenta.Text;
                Properties.Settings.Default.Contraseña = "";
                Properties.Settings.Default.Recordar = "no";
                Properties.Settings.Default.Save();
            }
        }

       

    }
}
