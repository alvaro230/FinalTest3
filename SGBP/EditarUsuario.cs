﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class EditarUsuario : FormBase
    {
        public EditarUsuario()
        {
            InitializeComponent();
        }

        private void EditarUsuario_Load(object sender, EventArgs e)
        {
            try
            {
                Usuario us = Usuarios.obtenerUsuarioPorCi(GestorUsuarios.usuarioEmpleadoCi);
                usuario.Text = us.Cuenta;
                contraseña.Text = us.Codigo;
            }
            catch (Exception)
            {

            }
        }

        private void guardar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario viejo = Usuarios.obtenerUsuarioPorCi(GestorUsuarios.usuarioEmpleadoCi);
                Usuario nuevo = new Usuario(usuario.Text.Trim(), contraseña.Text.Trim(), viejo.Rol, viejo.Empleado_ci);
                if (usuario.Text.Trim()!="" && contraseña.Text.Trim()!="" &&GestorUsuarios.validarEdicionUsuario(viejo,nuevo))
                {
                    Usuarios.editarUsuario(nuevo, viejo.Empleado_ci);

                    MessageBox.Show("Se guardaron los cambios", "Aviso");
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Nombre de usuario y/o contraseña invalidos", "Aviso");
                }
                
            }
            catch (Exception)
            {
                MessageBox.Show("No se guardaron los cambios","Aviso");
            }
        }
    }
}
