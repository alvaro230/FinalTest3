﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    public partial class CargaAsistencias : FormBase
    {
        public CargaAsistencias()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                string sFileName = choofdlog.FileName;
                ruta.Text = sFileName;   
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                double n;
                if (ruta.Text.Trim()!= "")
                {
                    if (double.TryParse(mes1.Text.Trim(), out n) && double.TryParse(año1.Text.Trim(), out n))
                    {
                        if (GestorAsistencias.validarFormatoArchivo(ruta.Text))
                        {
                            List<Asistencia> la = Asistencias.leerLineas(ruta.Text, mes1.Text, año1.Text);
                            List<Asistencia> cargados = Asistencias.guardarLineas(la);

                            foreach (Asistencia a in cargados)
                            {
                                dataGridView1.Rows.Add(a.Id, a.Fecha, a.Hora, a.Tipo);
                            }
                            correctos.Text = "Registros cargados: " + GestorAsistencias.corrrectos;
                            errores.Text = "Registros rechazados: " + GestorAsistencias.fallos;
                            
                            MessageBox.Show("Archivo cargado", "Aviso");

                            GestorAsistencias.corrrectos = 0;
                            GestorAsistencias.fallos = 0;
                        }
                        else
                        {
                            MessageBox.Show("El formato de archivo no es el correcto\nseleccione un archivo con formato .txt", "Aviso");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Datos del mes y año no especificados o incorrectos.", "Aviso");
                    }
                }
                else
                {
                    MessageBox.Show("Archivo no especificado, por favor introduzca la direccion\nhacia el archivo de asistencias.", "Aviso");
                }
            }
            catch (Exception)
            {
                
            }
             
        }

        private void CargaAsistencias_Load(object sender, EventArgs e)
        {
            dataGridView1.Columns.Add("ID", "Identificador");
            dataGridView1.Columns.Add("Fecha", "Fecha");
            dataGridView1.Columns.Add("Hora", "Hora");
            dataGridView1.Columns.Add("Tipo", "ENT/SAL");
            List<Asistencia> asistencias = Asistencias.obtenerAsistencias();
            if (asistencias.Count > 0)
            {
                mes1.Text = asistencias.First().Fecha.Substring(3, 2);
                año1.Text = asistencias.First().Fecha.Substring(6, 4);
            }
        }

        private void errores_Click(object sender, EventArgs e)
        {

        }
    }
}
