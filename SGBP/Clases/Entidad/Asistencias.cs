﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    class Asistencias
    {
        public static string id = "";

        public static List<Asistencia> leerLineas(string ruta, string mes, string año)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            List<Asistencia> asistenciasDeEseMes = obtenerAsistenciasPorMes(mes, año);
            List<Asistencia> resp = new List<Asistencia>();
            string line;

            System.IO.StreamReader archivo = new System.IO.StreamReader(@ruta);
            line = archivo.ReadLine();
            while ((line = archivo.ReadLine()) != null)
            {
                Asistencia a = deStringAAsistencia(line);
                if (a != null && a.Fecha.Substring(3, 2) == mes && a.Fecha.Substring(6, 4) == año &&
                   !GestorAsistencias.existeAsistencia(a, asistenciasDeEseMes))
                {
                    asistencias.Add(a);
                }
                else
                {
                    GestorAsistencias.fallos++;
                }
            }
            archivo.Close();
            return asistencias;
        }

        public static Asistencia deStringAAsistencia(string linea)
        {
            if (linea.Length > 0)
            {
                try
                {
                    int i = 0;
                    while (linea[i] != ' ')
                    {
                        i++;
                    }
                    string id = linea.Substring(0, 1);
                    int j = i + 1;
                    while (linea[j] != ' ')
                    {
                        j++;
                    }
                    string fecha = linea.Substring(i + 1, 10);
                    int k = j + 1;
                    while (linea[k] != ' ')
                    {
                        k++;
                    }
                    string hora = linea.Substring(j + 1, 5);

                    string tipo = linea.Substring(k + 1, 3);
                    return new Asistencia(id, fecha, hora, tipo);
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static List<Asistencia> guardarLineas(List<Asistencia> Asistencias)
        {
            List<Asistencia> resp=new List<Asistencia>();
            string dia = "";
            string mes = "";
            string año = "";
            string nuevafecha = "";
            foreach (Asistencia a in Asistencias)
            {
                if (GestorAsistencias.tienePar(a, Asistencias))
                {
                    dia = "" + a.Fecha[0] + a.Fecha[1];
                    mes = "" + a.Fecha[3] + a.Fecha[4];
                    año = "" + a.Fecha[6] + a.Fecha[7] + a.Fecha[8] + a.Fecha[9];
                    nuevafecha = "" + mes + FormBase.signoFecha + dia + FormBase.signoFecha + año;
                    string cmd = string.Format("insert into Asistencias(Id,Fecha,Hora,Tipo) values('{0}','{1}','{2}','{3}')",
                     a.Id, nuevafecha, a.Hora, a.Tipo);
                    Utilidades.EjecutarCambio(cmd);
                    GestorAsistencias.corrrectos++;
                    resp.Add(a);
                }else
                {
                    GestorAsistencias.fallos++;
                }
            }
            return resp;
        }



        public static List<Asistencia> obtenerAsistencias()
        {
            List<Asistencia> ls = new List<Asistencia>();
            DataSet DS = Utilidades.Ejecutar("select * from Asistencias order by id, Fecha DESC");
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                ls.Add(new Asistencia(DS.Tables[0].Rows[i]["Id"].ToString(),
                                      DS.Tables[0].Rows[i]["Fecha"].ToString().Substring(0, 10),
                                      DS.Tables[0].Rows[i]["Hora"].ToString(),
                                      DS.Tables[0].Rows[i]["Tipo"].ToString()));
            }
            return ls;
        }

        public static List<Asistencia> obtenerAsistenciasDeEmpleado(string id)
        {

            List<Asistencia> ls = new List<Asistencia>();
            string cmd = string.Format("select * from Asistencias where Id='{0}' order by Fecha DESC", id);
            DataSet DS = Utilidades.Ejecutar(cmd);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                ls.Add(new Asistencia(DS.Tables[0].Rows[i]["Id"].ToString(),
                                      DS.Tables[0].Rows[i]["Fecha"].ToString().Substring(0, 10),
                                      DS.Tables[0].Rows[i]["Hora"].ToString(),
                                      DS.Tables[0].Rows[i]["Tipo"].ToString()));
            }
            return ls;
        }

        public static List<Asistencia> obtenerAsistenciasPorMes(string mes, string año)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            List<Asistencia> respuestas = new List<Asistencia>();

            if (id == "")
            {
                asistencias = obtenerAsistencias();
            }
            else
            {
                asistencias = obtenerAsistenciasDeEmpleado(id);
            }

            string mesdata, añodata;
            foreach (Asistencia a in asistencias)
            {

                mesdata = "" + a.Fecha[3] + a.Fecha[4];
                añodata = "" + a.Fecha[6] + a.Fecha[7] + a.Fecha[8] + a.Fecha[9];
                if (mes == mesdata && año == añodata)
                {
                    respuestas.Add(new Asistencia(a.Id, a.Fecha, a.Hora, a.Tipo));
                }
            }

            return respuestas;
        }

        public static List<Asistencia> obtenerAsistenciasMesEmpleado(string id, string mes, string año)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            List<Asistencia> respuestas = new List<Asistencia>();

            asistencias = obtenerAsistenciasDeEmpleado(id);

            string mesdata, añodata;
            foreach (Asistencia a in asistencias)
            {

                mesdata = "" + a.Fecha[3] + a.Fecha[4];
                añodata = "" + a.Fecha[6] + a.Fecha[7] + a.Fecha[8] + a.Fecha[9];
                if (mes == mesdata && año == añodata)
                {
                    respuestas.Add(new Asistencia(a.Id, a.Fecha, a.Hora, a.Tipo));

                }
            }

            return respuestas;
        }

        public static List<Asistencia> obtenerAsistenciasMayorAEmpleado(string id, string dia,string mes, string año)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            List<Asistencia> respuestas = new List<Asistencia>();

            asistencias = obtenerAsistenciasDeEmpleado(id);

            string diadata,mesdata, añodata;
            foreach (Asistencia a in asistencias)
            {
                diadata = "" + a.Fecha[0] + a.Fecha[1];
                mesdata = "" + a.Fecha[3] + a.Fecha[4];
                añodata = "" + a.Fecha[6] + a.Fecha[7] + a.Fecha[8] + a.Fecha[9];
                if (Convert.ToInt32(diadata) >= Convert.ToInt32(dia) && mes == mesdata && año == añodata)
                {
                    respuestas.Add(new Asistencia(a.Id, a.Fecha, a.Hora, a.Tipo));

                }
            }

            return respuestas;
        }
        public static List<Asistencia> obtenerAsistenciasMenorAEmpleado(string id, string dia, string mes, string año)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            List<Asistencia> respuestas = new List<Asistencia>();

            asistencias = obtenerAsistenciasDeEmpleado(id);

            string diadata, mesdata, añodata;
            foreach (Asistencia a in asistencias)
            {
                diadata = "" + a.Fecha[0] + a.Fecha[1];
                mesdata = "" + a.Fecha[3] + a.Fecha[4];
                añodata = "" + a.Fecha[6] + a.Fecha[7] + a.Fecha[8] + a.Fecha[9];
                if (Convert.ToInt32(diadata) <= Convert.ToInt32(dia) && mes == mesdata && año == añodata)
                {
                    respuestas.Add(new Asistencia(a.Id, a.Fecha, a.Hora, a.Tipo));
                }
            }

            return respuestas;
        }

    }
}
