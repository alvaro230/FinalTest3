﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    class Usuarios
    {
        public static void crearUsuario(Usuario usuario)
        {

            string cmd2 = string.Format("insert into Usuarios(Cuenta,Codigo,Rol,Empleado_ci)" +
                "values('{0}','{1}','{2}','{3}')", usuario.Cuenta, usuario.Codigo, usuario.Rol, usuario.Empleado_ci);
            Utilidades.EjecutarCambio(cmd2);
        }

        public static void editarUsuario(Usuario usuario, string ciOrginal)
        {
            string cmd2 = string.Format("update Usuarios " +
                    "set Cuenta='{0}', " +
                    "Codigo='{1}', " +
                    "Rol='{2}', Empleado_ci='{3}' where Empleado_ci='{4}'",
                    usuario.Cuenta, usuario.Codigo, usuario.Rol, usuario.Empleado_ci, ciOrginal);
            Utilidades.EjecutarCambio(cmd2);
        }

        public static void eliminarUsuario(string empleado_ci)
        {
            string cmd2 = string.Format("delete from Usuarios where Empleado_ci='{0}'", empleado_ci);
            Utilidades.EjecutarCambio(cmd2);
        }
        public static Usuario obtenerUsuario(string usuario, string contraseña)
        {
            string CMD = string.Format("select * from Usuarios where Cuenta='{0}' and Codigo='{1}'",
                    usuario, contraseña);
            DataSet DS = Utilidades.Ejecutar(CMD);
            Usuario us = null;
            if (DS.Tables[0].Rows.Count > 0)
            {
                us = new Usuario(
                    DS.Tables[0].Rows[0]["Cuenta"].ToString(),
                    DS.Tables[0].Rows[0]["Codigo"].ToString(),
                    DS.Tables[0].Rows[0]["Rol"].ToString(),
                    DS.Tables[0].Rows[0]["Empleado_ci"].ToString());
            }
            return us;
        }

        public static bool obtenerTipoDeUsuario(Usuario usuario)
        {
            return Convert.ToBoolean(usuario.Rol);
        }

        public static Usuario obtenerUsuarioPorCi(string empleado_ci)
        {
            string cmd = string.Format("select * from Usuarios where Empleado_ci='{0}'", empleado_ci);
            DataSet DS = Utilidades.Ejecutar(cmd);
            Usuario us = new Usuario(
                DS.Tables[0].Rows[0]["Cuenta"].ToString(),
                DS.Tables[0].Rows[0]["Codigo"].ToString(),
                DS.Tables[0].Rows[0]["Rol"].ToString(),
                DS.Tables[0].Rows[0]["Empleado_ci"].ToString());
            return us;
        }

        public static List<Usuario> obtenerUsuarios()
        {
            string cmd = string.Format("select * from Usuarios");
            DataSet DS = Utilidades.Ejecutar(cmd);
            List<Usuario> us = new List<Usuario>();
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                us.Add(new Usuario(
                DS.Tables[0].Rows[i]["Cuenta"].ToString(),
                DS.Tables[0].Rows[i]["Codigo"].ToString(),
                DS.Tables[0].Rows[i]["Rol"].ToString(),
                DS.Tables[0].Rows[i]["Empleado_ci"].ToString()));

            }
            return us;
        }

    }
}
