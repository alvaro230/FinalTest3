﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    class Contactos
    {
       
        public static void crearContacto(Contacto emp)
        {
            string cmd = string.Format("insert into Contactos(nombreC,apellido,codigo,numero) " +
                "values('{0}','{1}','{2}','{3}');",
                emp.nombreC, emp.apellido, emp.codigo, emp.numero);
            Utilidades.EjecutarCambio(cmd);

        }
        public static List<Contacto> obtenerContactosPorCodigo(string codigo)
        {
            string cmd = string.Format("select * from Contactos  where codigo='{0}'", codigo.Trim());
            DataSet DS = Utilidades.Ejecutar(cmd);
            List<Contacto> us = new List<Contacto>();
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                us.Add(new Contacto(

                DS.Tables[0].Rows[i]["id"].ToString(),
                DS.Tables[0].Rows[i]["nombreC"].ToString(),
                DS.Tables[0].Rows[i]["apellido"].ToString(),
                DS.Tables[0].Rows[i]["codigo"].ToString(),
                DS.Tables[0].Rows[i]["numero"].ToString()));

            }
            return us;
        }


    }
}
