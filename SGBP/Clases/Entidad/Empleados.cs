﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    class Empleados
    {
       public static string formatoHora(string hora,string minuto)
        {
            int h = Convert.ToInt32(hora);
            int m = Convert.ToInt32(minuto);
            string reshora = hora;
            string resminuto = minuto;
            if (h < 10)
            {
                
                if (h == 0)
                {
                    reshora = "00";
                }
                else
                {
                    reshora = "0" + hora;
                }

            }
            if (m < 10)
            {
                
                if (m == 0)
                {
                    resminuto = "00";
                }else
                {
                    resminuto = "0" + minuto;
                }
            }
            return reshora + ":" + resminuto;
        }

        public static void crearEmpleado(Empleado emp)
        {
            string cmd = string.Format("insert into Empleados(Ci,Nombre,Apellido1,Apellido2,Cargo,FechaIngreso,Salario,Tipo,HoraEntrada,HoraSalida,Estado,id) " +
                "values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}');",
                emp.Ci, emp.Nombre, emp.Apellido1, emp.Apellido2, emp.Cargo, emp.FechaIngreso, emp.Salario,
                emp.Tipo, emp.HoraEntrada, emp.HoraSalida, emp.Estado, emp.id);
            Utilidades.EjecutarCambio(cmd);
        }

        public static void editarEmpleado(Empleado emp, string ciOriginal)
        {
            if (ciOriginal != "xyz123")
            {
                string cmd = string.Format("update Empleados " +
                        "set Ci='{0}', " +
                        "Nombre='{1}', " +
                        "Apellido1='{2}', " +
                        "Apellido2='{3}', " +
                        "Cargo='{4}', " +
                        "FechaIngreso='{5}', " +
                        "Salario='{6}', " +
                        "Tipo='{7}', " +
                        "HoraEntrada='{8}', " +
                        "HoraSalida='{9}', " +
                        "Estado='{10}', " +
                        "id='{11}' where Ci='{12}'",
                    emp.Ci, emp.Nombre, emp.Apellido1, emp.Apellido2, emp.Cargo,
                    emp.FechaIngreso, emp.Salario, emp.Tipo, emp.HoraEntrada, emp.HoraSalida,
                    emp.Estado, emp.id, ciOriginal);
                Utilidades.EjecutarCambio(cmd);
            }
        }

        public static void darDeBajaAEmpleado(string Ci, string estadonuevo)
        {
            if (Ci != "xyz123")
            {
                string cmd = string.Format("update Empleados set Estado='{0}' where Ci='{1}'", estadonuevo, Ci);
                Utilidades.EjecutarCambio(cmd);
            }
        }

        public static Empleado obtenerEmpleadoPorCi(string Ci)
        {

            string CMD = string.Format("select * from Empleados where Ci='{0}'", Ci.Trim());
            DataSet DS = Utilidades.Ejecutar(CMD);
            Empleado resp = null;
            if (DS.Tables[0].Rows.Count > 0)
            {
                 resp = new Empleado
                    (DS.Tables[0].Rows[0]["Ci"].ToString(),
                    DS.Tables[0].Rows[0]["Nombre"].ToString(),
                    DS.Tables[0].Rows[0]["Apellido1"].ToString(),
                    DS.Tables[0].Rows[0]["Apellido2"].ToString(),
                    DS.Tables[0].Rows[0]["Cargo"].ToString(),
                    DS.Tables[0].Rows[0]["FechaIngreso"].ToString().Substring(0, 10),
                    DS.Tables[0].Rows[0]["Salario"].ToString(),
                    DS.Tables[0].Rows[0]["Tipo"].ToString(),
                    DS.Tables[0].Rows[0]["HoraEntrada"].ToString(),
                    DS.Tables[0].Rows[0]["HoraSalida"].ToString(),
                    DS.Tables[0].Rows[0]["Estado"].ToString(),
                    DS.Tables[0].Rows[0]["id"].ToString());
            }
            return resp;
        }

        public static List<Empleado> obtenerEmpleados()
        {
            List<Empleado> empleados = new List<Empleado>();
            string CMD = string.Format("select * from Empleados");
            DataSet DS = Utilidades.Ejecutar(CMD);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (DS.Tables[0].Rows[i]["Ci"].ToString() != "xyz123")
                {
                    empleados.Add(new Empleado
                (DS.Tables[0].Rows[i]["Ci"].ToString(),
                DS.Tables[0].Rows[i]["Nombre"].ToString(),
                DS.Tables[0].Rows[i]["Apellido1"].ToString(),
                DS.Tables[0].Rows[i]["Apellido2"].ToString(),
                DS.Tables[0].Rows[i]["Cargo"].ToString(),
                DS.Tables[0].Rows[i]["FechaIngreso"].ToString().Substring(0, 10),
                DS.Tables[0].Rows[i]["Salario"].ToString(),
                DS.Tables[0].Rows[i]["Tipo"].ToString(),
                DS.Tables[0].Rows[i]["HoraEntrada"].ToString(),
                DS.Tables[0].Rows[i]["HoraSalida"].ToString(),
                DS.Tables[0].Rows[i]["Estado"].ToString(),
                DS.Tables[0].Rows[i]["id"].ToString()));
                }
            }
            return empleados;
        }

        public static List<Empleado> obtenerEmpleadosPorCiParecido(string ci)
        {
            List<Empleado> empleados = new List<Empleado>();
            string cmd = "select * from Empleados where Ci like '%" + ci + "%' or Nombre like '%" +
                ci + "%' or Apellido1 like '%" + ci + "%' or Apellido2 like '%" + ci + "%'";
            DataSet DS = Utilidades.Ejecutar(cmd);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (DS.Tables[0].Rows[i]["Ci"].ToString() != "xyz123")
                {
                    empleados.Add(new Empleado
                    (DS.Tables[0].Rows[i]["Ci"].ToString(),
                    DS.Tables[0].Rows[i]["Nombre"].ToString(),
                    DS.Tables[0].Rows[i]["Apellido1"].ToString(),
                    DS.Tables[0].Rows[i]["Apellido2"].ToString(),
                    DS.Tables[0].Rows[i]["Cargo"].ToString(),
                    DS.Tables[0].Rows[i]["FechaIngreso"].ToString().Substring(0, 10),
                    DS.Tables[0].Rows[i]["Salario"].ToString(),
                    DS.Tables[0].Rows[i]["Tipo"].ToString(),
                    DS.Tables[0].Rows[i]["HoraEntrada"].ToString(),
                    DS.Tables[0].Rows[i]["HoraSalida"].ToString(),
                    DS.Tables[0].Rows[i]["Estado"].ToString(),
                    DS.Tables[0].Rows[i]["id"].ToString()));
                }
            }
            return empleados;
        }

        public static Empleado obtenerEmpleadoPorIdAsistencia(string id)
        {
            string CMD = string.Format("select * from Empleados where id='{0}'", id);
            DataSet DS = Utilidades.Ejecutar(CMD);
            Empleado resp = new Empleado
                (DS.Tables[0].Rows[0]["Ci"].ToString(),
                DS.Tables[0].Rows[0]["Nombre"].ToString(),
                DS.Tables[0].Rows[0]["Apellido1"].ToString(),
                DS.Tables[0].Rows[0]["Apellido2"].ToString(),
                DS.Tables[0].Rows[0]["Cargo"].ToString(),
                DS.Tables[0].Rows[0]["FechaIngreso"].ToString().Substring(0, 10),
                DS.Tables[0].Rows[0]["Salario"].ToString(),
                DS.Tables[0].Rows[0]["Tipo"].ToString(),
                DS.Tables[0].Rows[0]["HoraEntrada"].ToString(),
                DS.Tables[0].Rows[0]["HoraSalida"].ToString(),
                DS.Tables[0].Rows[0]["Estado"].ToString(),
                DS.Tables[0].Rows[0]["id"].ToString());
            return resp;
        }

    }
}
