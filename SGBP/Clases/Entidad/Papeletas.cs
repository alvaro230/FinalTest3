﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    class Papeletas
    {

        public static DateTime stringAfecha(string fecha)
        {
            string dia = "" + fecha[0] + fecha[1];
            string mes = "" + fecha[3] + fecha[4];
            string año = "" + fecha[6] + fecha[7] + fecha[8] + fecha[9];
            return new DateTime(Convert.ToInt32(año), Convert.ToInt32(mes), Convert.ToInt32(dia));
        }

        public static bool fechaEntre(DateTime dini, DateTime d, DateTime dfin)
        {
            return (dini <= d && d <= dfin);

        }


        public static void crearPapeleta(Papeleta p)
        {
            string CMD4 = string.Format("insert into Papeletas(Empleado_ci,Cargo,Salario,FechaInicio,FechaFin,Descuento,Descuento_AFP,Pago) " +
                  "values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                  p.Ci, p.Cargo, p.Salario, p.FechaInicio, p.FechaFin, p.Descuentos, p.AFP, p.Pago);

            Utilidades.EjecutarCambio(CMD4);
        }


        public static Papeleta crearPapeletaFijo(string ci, string dia1, string mes1, string año1,
            string dia2, string mes2, string año2)
        {
            DateTime fechaini = new DateTime(Convert.ToInt32(año1), Convert.ToInt32(mes1), Convert.ToInt32(dia1));
            DateTime fechafin = new DateTime(Convert.ToInt32(año2), Convert.ToInt32(mes2), Convert.ToInt32(dia2));
            TimeSpan TS = fechafin - fechaini;

            Empleado empleado = Empleados.obtenerEmpleadoPorCi(ci);
            List<Asistencia> aux = Asistencias.obtenerAsistenciasDeEmpleado(empleado.id);

            List<Asistencia> asistenciasENT = new List<Asistencia>();
            List<Asistencia> asistenciasSAL = new List<Asistencia>();

            int diastrabajados = 0;
            double descuentos = 0;
            double descuentoAfp = 0;
            double sueldo = Convert.ToDouble(empleado.Salario);
            foreach (Asistencia a in aux)
            {
                if (fechaEntre(fechaini, stringAfecha(a.Fecha), fechafin))
                {
                    if (a.Tipo == "ENT")
                    {
                        asistenciasENT.Add(a);
                    }
                    else
                    {
                        asistenciasSAL.Add(a);
                    }
                }
            }

            List<Asistencia> RetrasosENT = new List<Asistencia>();
            List<Asistencia> AdelantosSAL = new List<Asistencia>();
            //clasificando asistencias fuera de las horas de trabajo
            foreach (Asistencia a in asistenciasENT)
            {
                int horaempleado = Convert.ToInt32(empleado.HoraEntrada.Substring(0,2));
                int horaregistro = Convert.ToInt32(a.Hora.Substring(0,2));
                int minutoempleado = Convert.ToInt32(empleado.HoraEntrada.Substring(3, 2));
                int minutoregistro = Convert.ToInt32(a.Hora.Substring(3, 2));
                if ((horaregistro>horaempleado) || (horaregistro==horaempleado && minutoregistro>minutoempleado*2))
                {
                    RetrasosENT.Add(a);
                }
            }
            foreach (Asistencia a in asistenciasSAL)
            {
                int horaempleado = Convert.ToInt32(empleado.HoraEntrada.Substring(0, 2));
                int horaregistro = Convert.ToInt32(a.Hora.Substring(0, 2));
                
                if (horaregistro < horaempleado)
                {
                
                    AdelantosSAL.Add(a);
                }
            }
            //remover de asistencias retrasos y adelantos
            foreach (Asistencia a in RetrasosENT)
            {
                asistenciasENT.Remove(a);
            }
            foreach (Asistencia a in AdelantosSAL)
            {
                asistenciasSAL.Remove(a);
            }
            //difefencia de asistencias entrada y salida
            if (asistenciasENT.Count > asistenciasSAL.Count)
            {
                diastrabajados = asistenciasENT.Count - (asistenciasENT.Count - asistenciasSAL.Count);
            }
            else
            {
                diastrabajados = asistenciasENT.Count - (asistenciasSAL.Count - asistenciasENT.Count);
            }
            //descontando
            descuentoAfp = sueldo * Properties.Settings.Default.AFP;
            if (Convert.ToBoolean(empleado.Estado))
            {
                if (diastrabajados < TS.Days - Properties.Settings.Default.Tolerancia)
                {
                    descuentos = sueldo * Properties.Settings.Default.Descuento;
                }
            }
            
            sueldo = sueldo - descuentos;
            sueldo = sueldo - descuentoAfp;
            //fecha para la base de datos
            string fechaini2 = fechaini.ToString("MM" + FormBase.signoFecha + "dd" + FormBase.signoFecha + "yyyy");
            string fechafin2 = fechafin.ToString("MM" + FormBase.signoFecha + "dd" + FormBase.signoFecha + "yyyy");
            
            crearPapeleta(new Papeleta(empleado.Ci, empleado.Cargo, empleado.Salario, fechaini2,
                 fechafin2, descuentos.ToString(), descuentoAfp.ToString(), sueldo.ToString()));
            //fecha para el formulario mostrarPapeletasGeneradas
            fechaini2= fechaini.ToString("dd" + FormBase.signoFecha + "MM" + FormBase.signoFecha + "yyyy");
            fechafin2 = fechafin.ToString("dd" + FormBase.signoFecha + "MM" + FormBase.signoFecha + "yyyy");
            Papeleta nuevapapeleta = new Papeleta(empleado.Ci, empleado.Cargo, empleado.Salario, fechaini2,
                 fechafin2, descuentos.ToString(), descuentoAfp.ToString(), sueldo.ToString());

            return nuevapapeleta;
        }


        public static Papeleta crearPapeletaHora(string ci, string dia1, string mes1, string año1,
            string dia2, string mes2, string año2)
        {

            DateTime fechaini = new DateTime(Convert.ToInt32(año1), Convert.ToInt32(mes1), Convert.ToInt32(dia1));
            DateTime fechafin = new DateTime(Convert.ToInt32(año2), Convert.ToInt32(mes2), Convert.ToInt32(dia2));

            Empleado empleado = Empleados.obtenerEmpleadoPorCi(ci);
            List<Asistencia> asistencias = Asistencias.obtenerAsistenciasDeEmpleado(empleado.id);

            double descuentos = 0;
            double descuentoAfp = 0;
            double sueldo = Convert.ToDouble(empleado.Salario);

            sueldo = sueldo * calcularHoras(asistencias, fechaini, fechafin);
            //fecha para la base de datos
            string fechaini2 = fechaini.ToString("MM" + FormBase.signoFecha + "dd" + FormBase.signoFecha + "yyyy");
            string fechafin2 = fechafin.ToString("MM" + FormBase.signoFecha + "dd" + FormBase.signoFecha + "yyyy");
            
            crearPapeleta(new Papeleta(empleado.Ci, empleado.Cargo, empleado.Salario, fechaini2,
                fechafin2, descuentos.ToString(), descuentoAfp.ToString(), sueldo.ToString()));
            //fecha para el formulario mostrarPapeletasGeneradas
            fechaini2 = fechaini.ToString("dd" + FormBase.signoFecha + "MM" + FormBase.signoFecha + "yyyy");
            fechafin2 = fechafin.ToString("dd" + FormBase.signoFecha + "MM" + FormBase.signoFecha + "yyyy");
            Papeleta nuevapapeleta = new Papeleta(empleado.Ci, empleado.Cargo, empleado.Salario, fechaini2,
                fechafin2, descuentos.ToString(), descuentoAfp.ToString(), sueldo.ToString());

            return nuevapapeleta;
        }

        public static int calcularHoras(List<Asistencia> asistencias, DateTime fechaini, DateTime fechafin)
        {
            int horastrabajadas = 0;
            int horasENT = 0;
            int horasSAL = 0;
            List<Asistencia> asistenciasENT = new List<Asistencia>();
            List<Asistencia> asistenciasSAL = new List<Asistencia>();
            foreach (Asistencia a in asistencias)
            {
                if (fechaEntre(fechaini, stringAfecha(a.Fecha), fechafin))
                {
                    if (a.Tipo == "ENT")
                    {
                        asistenciasENT.Add(a);
                    }
                    else
                    {
                        asistenciasSAL.Add(a);
                    }
                }
            }
            foreach (Asistencia a in asistenciasENT)
            {
                horasENT += Convert.ToInt32(a.Hora.Substring(0, 2));

            }
            foreach (Asistencia a in asistenciasSAL)
            {
                horasSAL += Convert.ToInt32(a.Hora.Substring(0, 2));
            }
            return horastrabajadas = horasSAL - horasENT;
        }

        public static List<Papeleta> crearPapeletasParaTodos(string dia1, string mes1, string año1, string dia2, string mes2, string año2)
        {
            List<Papeleta> papeletas = new List<Papeleta>();
            List<Empleado> empleados = Empleados.obtenerEmpleados();
            foreach (Empleado emp in empleados)
            {

                if (GestorPapeletas.validarCreacionPapeleta(emp.Ci, dia1, mes1, año1, dia2, mes2, año2))
                {
                    if (Convert.ToBoolean(emp.Tipo))
                    {

                        papeletas.Add(crearPapeletaFijo(emp.Ci, dia1, mes1, año1, dia2, mes2, año2));
                    }
                    else
                    {
                        papeletas.Add(crearPapeletaHora(emp.Ci, dia1, mes1, año1, dia2, mes2, año2));
                    }
                }
            }
            return papeletas;
        }


        public static List<Papeleta> ObtenerPapeletasDeEmpleado(string Ci)
        {
            List<Papeleta> papeletas = new List<Papeleta>();
            string CMD2 = string.Format("select * from Papeletas where Empleado_ci='{0}'", Ci);
            DataSet DS = Utilidades.Ejecutar(CMD2);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                papeletas.Add(new Papeleta
                (DS.Tables[0].Rows[i]["Empleado_ci"].ToString(),
                DS.Tables[0].Rows[i]["Cargo"].ToString(),
                DS.Tables[0].Rows[i]["Salario"].ToString(),
                DS.Tables[0].Rows[i]["FechaInicio"].ToString().Substring(0, 10),
                DS.Tables[0].Rows[i]["FechaFin"].ToString().Substring(0, 10),
                DS.Tables[0].Rows[i]["Descuento"].ToString(),
                DS.Tables[0].Rows[i]["Descuento_AFP"].ToString(),
                DS.Tables[0].Rows[i]["Pago"].ToString()));
            }
            return papeletas;
        }

        public static List<Papeleta> ObtenerTodasLasPapeletas()
        {
            List<Papeleta> papeletas = new List<Papeleta>();
            string CMD2 = string.Format("select * from Papeletas");
            DataSet DS = Utilidades.Ejecutar(CMD2);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                papeletas.Add(new Papeleta
                (DS.Tables[0].Rows[i]["Empleado_ci"].ToString(),
                DS.Tables[0].Rows[i]["Cargo"].ToString(),
                DS.Tables[0].Rows[i]["Salario"].ToString(),
                DS.Tables[0].Rows[i]["FechaInicio"].ToString().Substring(0, 10),
                DS.Tables[0].Rows[i]["FechaFin"].ToString().Substring(0, 10),
                DS.Tables[0].Rows[i]["Descuento"].ToString(),
                DS.Tables[0].Rows[i]["Descuento_AFP"].ToString(),
                DS.Tables[0].Rows[i]["Pago"].ToString()));
            }
            return papeletas;
        }

        public static List<Papeleta> obtenerPapeletasPorEmpleadoParecido(string criterio)
        {
            List<Papeleta> papeletas = new List<Papeleta>();
            string CMD = "select * from Papeletas where Empleado_ci like '%" + criterio + "%'";
            DataSet DS = Utilidades.Ejecutar(CMD);

            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                papeletas.Add(new Papeleta
               (DS.Tables[0].Rows[i]["Empleado_ci"].ToString(),
               DS.Tables[0].Rows[i]["Cargo"].ToString(),
               DS.Tables[0].Rows[i]["Salario"].ToString(),
               DS.Tables[0].Rows[i]["FechaInicio"].ToString().Substring(0, 10),
               DS.Tables[0].Rows[i]["FechaFin"].ToString().Substring(0, 10),
               DS.Tables[0].Rows[i]["Descuento"].ToString(),
               DS.Tables[0].Rows[i]["Descuento_AFP"].ToString(),
               DS.Tables[0].Rows[i]["Pago"].ToString()));
            }
            return papeletas;
        }
    }
}
