﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGBP
{
    class Viajes
    {

        public static void crearViaje(Viaje emp)
        {
            string cmd = string.Format("insert into Viajes(nombre,Empleado_ci,Historia) " +
                "values('{0}','{1}','{2}')", emp.nombre, emp.Empleado_ci,emp.Historia);
            Utilidades.EjecutarCambio(cmd);
           // MessageBox.Show("El empleado se registro exitosamente","Aviso");

        }
        /*
                public static List<Empleado> obtenerViajes()
                {
                    List<Viaje> viajes = new List<Viaje>();
                    string CMD = string.Format("select * from Viajes");
                    DataSet DS = Utilidades.Ejecutar(CMD);
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        if (DS.Tables[0].Rows[i]["nombre"].ToString() != "xyz123")
                        {
                            viajes.Add(new Viaje
                        (DS.Tables[0].Rows[i]["nombre"].ToString(),
                        DS.Tables[0].Rows[i]["Empleado_ci"].ToString()));
                        }
                    }
                    return viajes;
                }

            */


        public static List<Viaje> obtenerViajes()
        {
            List<Viaje> viajes = new List<Viaje>();
            string CMD = string.Format("select * from Viajes");
            DataSet DS = Utilidades.Ejecutar(CMD);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
               
                  viajes.Add(new Viaje
                (DS.Tables[0].Rows[i]["Id"].ToString(),
                      DS.Tables[0].Rows[i]["nombre"].ToString(),
                DS.Tables[0].Rows[i]["Empleado_ci"].ToString(), 
                DS.Tables[0].Rows[i]["Historia"].ToString()));
               
            }
            return viajes;
        }

         public static Viaje obtenerViajesPorId(string id)
        {

            string CMD = string.Format("select * from Viajes where Id='{0}'", id.Trim());
            DataSet DS = Utilidades.Ejecutar(CMD);
            Viaje resp = null;
            if (DS.Tables[0].Rows.Count > 0)
            {
                 resp = new Viaje
                    (
                    DS.Tables[0].Rows[0]["Nombre"].ToString(),
                    DS.Tables[0].Rows[0]["Empleado_ci"].ToString(), 
                    DS.Tables[0].Rows[0]["Historia"].ToString());
            }
            return resp;
        }
    }
}
