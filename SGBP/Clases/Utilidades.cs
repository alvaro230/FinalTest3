﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace SGBP
{
    public class Utilidades
    {

        

        public static DataSet Ejecutar(string cmd)
        {
            SqlConnection con = new SqlConnection(Properties.Settings.Default.Coneccion);
            con.Open();

            DataSet DS = new DataSet();
            SqlDataAdapter DP = new SqlDataAdapter(cmd,con);
            
         
            DP.Fill(DS);
            
            con.Close();

           
            return DS;

        }

        public static void EjecutarCambio(string cmd)
        {
            SqlConnection con = new SqlConnection(Properties.Settings.Default.Coneccion);
            con.Open();
            SqlCommand myCommand = new SqlCommand(cmd, con);
            myCommand.ExecuteNonQuery();
        }
    }
}
