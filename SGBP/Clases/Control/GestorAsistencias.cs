﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace SGBP
{
    class GestorAsistencias
    {

        
        public static int corrrectos = 0;
        public static int fallos = 0;
        public static bool validarFormatoArchivo(string ruta)
        {
            bool resp = false;
            if (ruta.Substring(ruta.Length - 4, 4)==".txt")
            {
                resp = true;
            }
            return resp;
        }

        public static bool existeAsistencia(Asistencia asistencia,List<Asistencia> asistencias)
        {
            
            foreach(Asistencia a in asistencias)
            {
                string fechalista = a.Fecha.Substring(0, 2) + FormBase.signoFecha + a.Fecha.Substring(3, 2) + FormBase.signoFecha + a.Fecha.Substring(6, 4);
                string fechaasistencia = asistencia.Fecha.Substring(0, 2) + FormBase.signoFecha + asistencia.Fecha.Substring(3, 2) + FormBase.signoFecha + asistencia.Fecha.Substring(6, 4);

                if (asistencia.Id == a.Id && fechaasistencia == fechalista && asistencia.Hora==a.Hora &&
                    asistencia.Tipo == a.Tipo)
                {
                    return true;
                }
            }
            return false;
        }
        

        public static bool tienePar(Asistencia asistencia,List<Asistencia> lista)
        {
            bool resp = false;
            
            foreach(Asistencia a in lista)
            {
                string fechalista = a.Fecha.Substring(0,2)+FormBase.signoFecha+ a.Fecha.Substring(3, 2) + FormBase.signoFecha+ a.Fecha.Substring(6, 4);
                string fechaasistencia = asistencia.Fecha.Substring(0, 2) + FormBase.signoFecha + asistencia.Fecha.Substring(3, 2) + FormBase.signoFecha + asistencia.Fecha.Substring(6, 4);
                if (fechalista == fechaasistencia && a.Id==asistencia.Id)
                {
                    if(asistencia.Tipo=="ENT" && a.Tipo=="SAL")
                    {
                        resp = true;
                    }
                    else
                    {
                        if(asistencia.Tipo == "SAL" && a.Tipo == "ENT")
                        {
                            resp = true;
                        }
                    }
                  
                }
            }
            return resp;
        }

        

    }
}
