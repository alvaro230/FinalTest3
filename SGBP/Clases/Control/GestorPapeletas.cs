﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace SGBP
{
    class GestorPapeletas
    {
        public static string horas = "";
        

        public static bool validarCreacionPapeleta(string ci, string dia1, string mes1, string año1, 
            string dia2, string mes2, string año2)
        {
            DateTime fecha1 = new DateTime(Convert.ToInt32(año1), Convert.ToInt32(mes1), Convert.ToInt32(dia1));
            DateTime fecha2 = new DateTime(Convert.ToInt32(año2), Convert.ToInt32(mes2), Convert.ToInt32(dia2));
            
            List<Papeleta> papeletas = Papeletas.ObtenerPapeletasDeEmpleado(ci);
            Empleado empleado = Empleados.obtenerEmpleadoPorCi(ci);
            bool resp = true;
            if (Convert.ToBoolean(empleado.Estado))
            {
                
                if (Asistencias.obtenerAsistenciasMayorAEmpleado(empleado.id,dia1, mes1, año1).Count > 0
                    && Asistencias.obtenerAsistenciasMenorAEmpleado(empleado.id,dia2, mes2, año2).Count > 0)
                {
                    if (papeletas != null)
                    {
                        foreach (Papeleta p in papeletas)
                        {
                            DateTime f1 = Papeletas.stringAfecha(p.FechaInicio);
                            DateTime f2 = Papeletas.stringAfecha(p.FechaFin);

                            if (Papeletas.fechaEntre(f1, fecha1, f2) || Papeletas.fechaEntre(f1, fecha2, f2) ||
                                Papeletas.fechaEntre(fecha1, f1, fecha2) || Papeletas.fechaEntre(fecha1, f2, fecha2))
                            {
                                resp = false;
                            }
                        }
                    }
                }
                else
                {
                    resp = false;
                }
            }else
            {
              
                resp = false;
            }
            return resp;
        }

       

        

        public static void PdfFijo(string Ci,string nombre,string apellido1,string apellido2,
            string cargo,string salario,string descuentos,string afp,string total, string fecha1,string fecha2 )
        {
            
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            // Show the FolderBrowserDialog.
            DialogResult result = folderBrowserDialog1.ShowDialog();
            string folderName = "";
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath + "/";

                Document doc = new Document(iTextSharp.text.PageSize.LETTER, 30, 30, 10, 10);
                iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(450, 300);
                doc.SetPageSize(rec);
                PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(@folderName + "Papeleta_Empleado_"+ Ci + fecha2.Substring(3, 2) + fecha2.Substring(6, 4) + ".pdf", FileMode.Create));
                iTextSharp.text.Font titulofuente =
                    new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15, 1);
                iTextSharp.text.Font textofuente =
                    new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10);
                doc.Open();

                Paragraph titulo = new Paragraph("ASOCIADOS INFORMATICOS\nPapeleta de pago\n", titulofuente);
                titulo.Alignment = Element.ALIGN_CENTER;
                

                Paragraph parrafo1 = new Paragraph
                    ("Empleado                                             "
                        + "Periodo: Del " + fecha1.Trim() + " Al " + fecha2.Trim(), textofuente);
                Paragraph parrafo2 = new Paragraph
                    (nombre.Trim() + "     " +
                    apellido1.Trim() + "     " +
                    apellido2.Trim() + "       " +
                    "Cargo:                   " + cargo.Trim() + "\n\n", textofuente);
                Paragraph parrafo3 = new Paragraph
                    ("Salario basico:         "  + salario.Trim() + "\n\n", textofuente);
                Paragraph parrafo4 = new Paragraph
                    ("Descuentos:             "  + descuentos.Trim() + "\n\n", textofuente);
                Paragraph parrafo5 = new Paragraph
                    ("AFP:                       " + afp.Trim() + "\n\n", textofuente);
                Paragraph parrafo6 = new Paragraph
                    ("Pago total:             "  + total.Trim() + "\n", textofuente);

                //doc.NewPage();
                doc.Add(titulo);
                doc.Add(parrafo1);
                doc.Add(parrafo2);
                doc.Add(parrafo3);
                doc.Add(parrafo4);
                doc.Add(parrafo5);
                doc.Add(parrafo6);
                doc.Close();
            }
        }

        public static void PdfHora(string Ci, string nombre, string apellido1, string apellido2,
            string cargo, string salario,string horas,string descuentos,
            string afp, string total, string fecha1, string fecha2 )
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            // Show the FolderBrowserDialog.
            DialogResult result = folderBrowserDialog1.ShowDialog();
            string folderName = "";
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath + "/";

                Document doc = new Document(iTextSharp.text.PageSize.LETTER, 30, 30, 10, 10);
                iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(450, 300);
                doc.SetPageSize(rec);
                PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(@folderName + "Papeleta_Empleado_" + Ci +fecha2.Substring(3,2)+fecha2.Substring(6,4) + ".pdf", FileMode.Create));
                iTextSharp.text.Font titulofuente =
                    new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15, 1);
                iTextSharp.text.Font textofuente =
                    new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10);
                doc.Open();

                Paragraph titulo = new Paragraph("ASOCIADOS INFORMATICOS\nPapeleta de pago\n", titulofuente);
                titulo.Alignment = Element.ALIGN_CENTER;
                //"                                   "

                Paragraph parrafo1 = new Paragraph
                    ("Empleado                                             "
                        + "Periodo: Del " + fecha1.Trim() + " Al " + fecha2.Trim(), textofuente);
                Paragraph parrafo2 = new Paragraph
                    (nombre.Trim() + "     " +
                    apellido1.Trim() + "     " +
                    apellido2.Trim() + "       " +
                    "Cargo:         " + cargo.Trim() + "\n\n", textofuente);
                Paragraph parrafo3 = new Paragraph
                    ("Salario basico:           " + salario.Trim() + "\n\n", textofuente);
                Paragraph parrafo4 = new Paragraph
                   ("Horas Trabajadas:          " + horas.Trim() + "\n\n", textofuente);
                Paragraph parrafo5 = new Paragraph
                    ("Descuentos:           " + descuentos.Trim() + "\n\n", textofuente);
                Paragraph parrafo6 = new Paragraph
                    ("AFP:          " + afp.Trim() + "\n\n", textofuente);
                Paragraph parrafo7 = new Paragraph
                    ("Pago total:           " + total.Trim() + "\n", textofuente);

                //doc.NewPage();
                doc.Add(titulo);
                doc.Add(parrafo1);
                doc.Add(parrafo2);
                doc.Add(parrafo3);
                doc.Add(parrafo4);
                doc.Add(parrafo5);
                doc.Add(parrafo6);
                doc.Add(parrafo7);
                doc.Close();
            }
        }

        public static void PdfTodos(string dia1,string mes1,string año1,string dia2,string mes2,string año2)
        {
            if (MessageBox.Show("Confirma la impresion en pdf de " +
               "\ntodas las papeletas generadas? ", "Aviso", MessageBoxButtons.YesNo
               , MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                string fecini = string.Format(dia1 + FormBase.signoFecha + mes1 + FormBase.signoFecha + año1);
                string fecfin = string.Format(dia2 + FormBase.signoFecha + mes2 + FormBase.signoFecha + año2);
                List<Papeleta> papeletas = Papeletas.ObtenerTodasLasPapeletas();
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                // Show the FolderBrowserDialog.
                DialogResult result = folderBrowserDialog1.ShowDialog();
                string folderName = "";
                if (result == DialogResult.OK)
                {
                    
                    Document doc = new Document(iTextSharp.text.PageSize.LETTER, 30, 30, 10, 10);
                    iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(450, 300);
                    doc.SetPageSize(rec);
                    PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(@folderName + "Papeletas_Empleados_" + mes2 + año2 + ".pdf", FileMode.Create));
                    iTextSharp.text.Font titulofuente =
                        new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15, 1);
                    iTextSharp.text.Font textofuente =
                        new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10);
                    doc.Open();

                    foreach (Papeleta p in papeletas)
                    {
                        if (p.FechaInicio == fecini && p.FechaFin == fecfin)
                        {
                            Empleado emp = Empleados.obtenerEmpleadoPorCi(p.Ci);
                            Paragraph titulo = new Paragraph("ASOCIADOS INFORMATICOS\nPapeleta de pago\n", titulofuente);
                            titulo.Alignment = Element.ALIGN_CENTER;
                            //"                                   "
                            Paragraph parrafo1 = new Paragraph
                                ("Empleado                                             "
                                        + "Periodo: Del " + fecini + " Al " + fecfin, textofuente);
                            Paragraph parrafo2 = new Paragraph
                                (emp.Nombre + "     " +
                                emp.Apellido1 + "     " +
                                emp.Apellido2 + "       " +
                                "Cargo: " + p.Cargo + "\n\n", textofuente);
                            Paragraph parrafo3 = new Paragraph
                                ("Salario basico:               " + p.Salario + "\n\n", textofuente);
                            Paragraph parrafo4 = new Paragraph
                                ("", textofuente);
                            if (Convert.ToBoolean(emp.Tipo))
                            {

                                int horas = Papeletas.calcularHoras
                                    (Asistencias.obtenerAsistenciasDeEmpleado(emp.id),
                                    Papeletas.stringAfecha(p.FechaInicio),
                                    Papeletas.stringAfecha(p.FechaFin));
                                parrafo4 = new Paragraph
                                ("Horas Trabajadas:          " + horas + "\n\n", textofuente);
                            }
                            Paragraph parrafo5 = new Paragraph
                                ("Descuentos:                   " + p.Descuentos + "\n\n", textofuente);
                            Paragraph parrafo6 = new Paragraph
                                ("AFP:                          " + p.AFP + "\n\n", textofuente);
                            Paragraph parrafo7 = new Paragraph
                                ("Pago total:                   " + p.Pago + "\n", textofuente);

                            doc.Add(titulo);
                            doc.Add(parrafo1);
                            doc.Add(parrafo2);
                            doc.Add(parrafo3);
                            doc.Add(parrafo4);
                            doc.Add(parrafo5);
                            doc.Add(parrafo6);
                            doc.Add(parrafo7);
                            doc.NewPage();
                        }
                    }
                    doc.Close();
                }
            }
        }
    }
}
