﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace SGBP
{
    public class GestorUsuarios
    {

        public static string usuarioEmpleadoCi = "";

        public static bool comparar(string string1, string string2)
        {
            if (String.Compare(string1, string2, StringComparison.OrdinalIgnoreCase) == 0)
            {
                return true;
            }
            return false;
        }

        public static bool validarIngresoUsuario(Usuario usuario,string cuenta,string codigo)
        {
                if (comparar(cuenta,usuario.Cuenta) && comparar(codigo, usuario.Codigo))
                {
                    Empleado emp = Empleados.obtenerEmpleadoPorCi(usuario.Empleado_ci);
                    if (Convert.ToBoolean(emp.Estado) || Convert.ToBoolean(usuario.Rol))
                    {
                        return true;
                    }else
                    {
                        MessageBox.Show("El empleado de este usuario esta dado de baja", "Aviso");
                    }
                }
            return false;
        }

        public static bool validarCreacionUsuario(Usuario usuario)
        {
            return validarCuentaUsuario(usuario);
        }

        public static bool validarEdicionUsuario(Usuario usuarioViejo, Usuario usuarioNuevo)
        {
            List<Usuario> usuarios = Usuarios.obtenerUsuarios();
            
                if (usuarios.Count > 0)
                {
                    usuarios.Remove(usuarioViejo);
                    foreach (Usuario u in usuarios)
                    {
                        if (comparar(u.Cuenta,usuarioNuevo.Cuenta) && !comparar(usuarioViejo.Cuenta,usuarioNuevo.Cuenta))
                        {
                            return false;
                        }
                    }
                }
            
            return true;
        }

        public static bool validarCuentaUsuario(Usuario usuario)
        {
            List<Usuario> usuarios = Usuarios.obtenerUsuarios();
            if (usuario.Cuenta != "" && usuario.Codigo!="")
            {
                foreach (Usuario u in usuarios)
                {
                    if (comparar(u.Cuenta,usuario.Cuenta))
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public static bool existeAdministradores()
        {
            List<Usuario> usuarios = Usuarios.obtenerUsuarios();
            foreach(Usuario u in usuarios)
            {
                if (Convert.ToBoolean(u.Rol))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool existeLaCuenta(string cuenta)
        {
            List<Usuario> usuarios = Usuarios.obtenerUsuarios();
            if (usuarios.Count > 0)
            {
                foreach (Usuario u in usuarios)
                {
                    if (comparar(u.Cuenta,cuenta))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static void verificarForm(Form frmhijo, Form frmpapa)
        {
            bool cargado = false;
            foreach (Form llamado in frmpapa.MdiChildren)
            {
                if (llamado.Text == frmhijo.Text)
                {
                    cargado = true;
                    break;
                }
            }
            if (!cargado)
            {
                frmhijo.MdiParent = frmpapa;
                frmhijo.Dock = DockStyle.Fill;

                frmhijo.Show();
            }
        }

        public static void CerrarTodos(Form frmpapa)
        {
            foreach (Form hijo in frmpapa.MdiChildren)
            {
                hijo.Close();
            }
        }

    }
}
