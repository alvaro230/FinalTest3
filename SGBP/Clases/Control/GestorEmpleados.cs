﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace SGBP
{
    class GestorEmpleados
    {
        public static bool validarCreacionEmpleado(Empleado emp)
        {
            if (emp.Ci != "")
            {
                if (Empleados.obtenerEmpleadoPorCi(emp.Ci) == null)
                {
                    if (emp.Nombre.Length > 0 && (emp.Apellido1.Length > 0 || emp.Apellido2.Length > 0))
                    {
                        double n;
                        if (double.TryParse(emp.Salario, out n) && validarIdAsistenciaEmpleado(emp))
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Datos de salario o identificador de asistencia invalidos.", "Aviso");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar al menos el nombre del empleado.", "Aviso");
                    }
                }
                else
                {
                    MessageBox.Show("Ya existe un empleado con el Ci ingresado\n por favor ingrese otro.", "Aviso");
                }
            }else
            {
                MessageBox.Show("Debe ingresar el Ci del empleado", "Aviso");
            }
            return false;
        }

        public static bool validarIdAsistenciaEmpleado(Empleado emp)
        {
            List<Empleado> empleados = Empleados.obtenerEmpleados();
            int n;
            if (int.TryParse(emp.id, out n))
            {
                foreach (Empleado e in empleados)
                {
                    if (e.id == emp.id)
                    {
                        return false;
                    }
                }
            }else
            {
                return false;
            }
            return true;
        }

        public static bool validarIdAsistenciaEditado(Empleado viejo,Empleado nuevo)
        {
            List<Empleado> empleados = Empleados.obtenerEmpleados();
            int n;
            if (int.TryParse(nuevo.id, out n))
            {
                foreach (Empleado e in empleados)
                {
                    if (e.id==nuevo.id && nuevo.id!=viejo.id)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public static bool validarEdicionDeEmpleado(Empleado viejo,Empleado nuevo)
        {
            if (nuevo.Ci != "")
            {
                if (Empleados.obtenerEmpleadoPorCi(nuevo.Ci) == null || comparar(nuevo.Ci,viejo.Ci))
                {
                    if (nuevo.Nombre.Length > 0 && (nuevo.Apellido1.Length > 0 || nuevo.Apellido2.Length > 0))
                    {
                        double n;
                        if (double.TryParse(nuevo.Salario, out n) && validarIdAsistenciaEditado(viejo,nuevo))
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Datos de salario o identificador de asistencia invalidos.", "Aviso");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar al menos el nombre del empleado.", "Aviso");
                    }
                }
                else
                {
                    MessageBox.Show("Ya existe un empleado con el Ci ingresado\n por favor ingrese otro.", "Aviso");
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar el Ci del empleado", "Aviso");
            }
            return false;
        }
        public static bool comparar(string string1, string string2)
        {
            if (String.Compare(string1, string2, StringComparison.OrdinalIgnoreCase) == 0)
            {
                return true;
            }
            return false;
        }
    }

  
    
}
