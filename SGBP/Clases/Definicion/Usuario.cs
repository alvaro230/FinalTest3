﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    public class Usuario
    {
        public string Cuenta = "";
        public string Codigo = "";
        public string Rol = "";
        public string Empleado_ci = "";

        public Usuario(string Cuenta,string Codigo,string Rol,string Empleado_ci)
        {
            this.Cuenta = Cuenta;
            this.Codigo = Codigo;
            this.Rol = Rol;
            this.Empleado_ci = Empleado_ci;
        }
    }
}
