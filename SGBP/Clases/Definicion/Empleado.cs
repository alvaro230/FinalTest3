﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    class Empleado
    {
        public string Ci = "", Nombre = "", Apellido1 = "", Apellido2 = "", Cargo = "",
            FechaIngreso = "", Salario = "", Tipo = "", HoraEntrada = "", HoraSalida = "", Estado = "", id="";
        
        public Empleado(string Ci, string Nombre, string Apellido1 , string Apellido2,
            string Cargo,string FechaIngreso, string Salario, string Tipo,string HoraEntrada,string HoraSalida,
            string Estado, string id)
        {
            this.Ci = Ci;
            this.Nombre = Nombre;
            this.Apellido1 = Apellido1;
            this.Apellido2 = Apellido2;
            this.Cargo = Cargo;
            this.FechaIngreso = FechaIngreso;
            this.Salario = Salario;
            this.Tipo = Tipo;
            this.HoraEntrada = HoraEntrada;
            this.HoraSalida = HoraSalida;
            this.Estado = Estado;
            this.id = id;
        }
    }
}
