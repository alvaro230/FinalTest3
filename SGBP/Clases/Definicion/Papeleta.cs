﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBP
{
    public class Papeleta
    {
        public string Ci = "";
        public string Cargo = "";
        public string Salario = "";
        public string FechaInicio = "";
        public string FechaFin = "";
        public string Descuentos = "";
        public string AFP = "";
        public string Pago = "";
        public Papeleta(string Ci, string Cargo, string Salario, string FechaInicio, string FechaFin, 
            string Descuentos,string AFP, string Pago)
        {
            this.Ci = Ci;
            this.Cargo = Cargo;
            this.Salario = Salario;
            this.FechaInicio = FechaInicio;
            this.FechaFin = FechaFin;
            this.Descuentos = Descuentos;
            this.AFP = AFP;
            this.Pago = Pago;
        }

    }
}
